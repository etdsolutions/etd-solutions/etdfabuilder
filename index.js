const path = require('path')
const fs = require('fs')

const EtdFaBuilderError = require('./lib/error')

var filenessCache = { }
var configCache = { }

function isFile(file) {
    if (file in filenessCache) {
        return filenessCache[file]
    }
    var result = fs.existsSync(file) && fs.statSync(file).isFile()
    if (!process.env.ETDFABUILDER_DISABLE_CACHE) {
        filenessCache[file] = result
    }
    return result
}

function eachParent(file, callback) {
    var loc = path.resolve(file)
    do {
        var result = callback(loc)
        if (typeof result !== 'undefined') return result
    } while (loc !== (loc = path.dirname(loc)))
    return undefined
}

function parsePackage(file) {
    var config = JSON.parse(fs.readFileSync(file))
    var list = config.etdfabuilder
    if (typeof list === 'object' && list.length) {
        list = { defaults: list }
    }
    return list
}

function findConfig(from) {
    from = path.resolve(from)

    var cacheKey = isFile(from) ? path.dirname(from) : from
    if (cacheKey in configCache) {
        return configCache[cacheKey]
    }

    var resolved = eachParent(from, function(dir) {
        var pkg = path.join(dir, 'package.json')

        var pkgEtdfabuilder
        if (isFile(pkg)) {
            try {
                pkgEtdfabuilder = parsePackage(pkg)
            } catch (e) {
                if (e.name === 'EtdFaBuilderError') throw e
                console.warn(
                    '[EtdFaBuilder] Could not parse ' + pkg + '. Ignoring it.')
            }
        }

        return pkgEtdfabuilder
    })
    if (!process.env.ETDFABUILDER_DISABLE_CACHE) {
        configCache[cacheKey] = resolved
    }
    return resolved
}

function loadConfig(current_path) {
    if (current_path) {
        return findConfig(current_path)
    } else {
        return undefined
    }
}

module.exports = function(output) {

    const current_path = path.resolve ? path.resolve('.') : '.';
    const config = loadConfig(current_path);
    const NAMESPACE_IDENTIFIER = '___FONT_AWESOME___';

    if (config === undefined) {
        throw new EtdFaBuilderError("Unable to find 'etdfabuilder' config in any package.json");
    }

    let icons   = {};
    let defines = '';

    // On parcourt chacune des collections d'icones
    Object.keys(config).forEach((key) => {

        // On contrôle l'existance du package demandé.
        try {
            require("@fortawesome/fontawesome-" + key);
        } catch (e) {
            throw new EtdFaBuilderError("Unable to find @fortawesome/fontawesome-" + key + " dependency. Please add it to your package.json and install it.")
        }

        // On parcourt les icones demandés dans la collection
        Object.keys(config[key]).forEach((collection_name) => {

            let js_file;

            try {
                js_file = require.resolve("@fortawesome/fontawesome-" + key + "/js/" + collection_name)
            } catch (e) {
                throw new EtdFaBuilderError("Cannot find @fortawesome/fontawesome-" + key + "/js/" + collection_name + " dependency. Please check the style name.")
            }

            // On réinitialise l'objet window.
            global.window = {};

            // On charge le fichier JS et on l'exécute.
            let js = fs.readFileSync(js_file, 'utf8');
            eval(js);

            // On récupère le préfixe.
            let prefix = Object.keys(global.window[NAMESPACE_IDENTIFIER].styles)[0];

            // On ne garde que les icones voulues.
            let all      = global.window[NAMESPACE_IDENTIFIER].styles[prefix];
            let wanted   = config[key][collection_name];
            let filtered = Object.keys(all)
                .filter(key => wanted.includes(key))
                .reduce((obj, key) => {
                    obj[key] = all[key];
                    return obj;
                }, {});


            if (!icons[prefix]) {
                defines += `bunker(function () {
  define('${prefix}', icons_${prefix});
});\n`;
            }
            icons[prefix] = Object.assign(icons[prefix] || {}, filtered);

        });

    });

    let icons_str = '';

    Object.keys(icons).forEach((prefix) => {
         icons_str += `var icons_${prefix} = ${JSON.stringify(icons[prefix])};\n`
    });

    let js = `
    
(function () {
'use strict';

var _WINDOW = {};
try {
  if (typeof window !== 'undefined') _WINDOW = window;
  
} catch (e) {}

var _ref = _WINDOW.navigator || {};
var _ref$userAgent = _ref.userAgent;
var userAgent = _ref$userAgent === undefined ? '' : _ref$userAgent;
var WINDOW = _WINDOW;
var IS_IE = ~userAgent.indexOf('MSIE') || ~userAgent.indexOf('Trident/');
var NAMESPACE_IDENTIFIER = '___FONT_AWESOME___';
var PRODUCTION = function () {
  try {
    return undefined === 'production';
  } catch (e) {
    return false;
  }
}();
var oneToTen = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var oneToTwenty = oneToTen.concat([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
var RESERVED_CLASSES = ['xs', 'sm', 'lg', 'fw', 'ul', 'li', 'border', 'pull-left', 'pull-right', 'spin', 'pulse', 'rotate-90', 'rotate-180', 'rotate-270', 'flip-horizontal', 'flip-vertical', 'stack', 'stack-1x', 'stack-2x', 'inverse', 'layers', 'layers-text', 'layers-counter'].concat(oneToTen.map(function (n) {
  return n + 'x';
})).concat(oneToTwenty.map(function (n) {
  return 'w-' + n;
}));

function bunker(fn) {
  try {
    fn();
  } catch (e) {
    if (!PRODUCTION) {
      throw e;
    }
  }
}

var w = WINDOW || {};

if (!w[NAMESPACE_IDENTIFIER]) w[NAMESPACE_IDENTIFIER] = {};
if (!w[NAMESPACE_IDENTIFIER].styles) w[NAMESPACE_IDENTIFIER].styles = {};
if (!w[NAMESPACE_IDENTIFIER].hooks) w[NAMESPACE_IDENTIFIER].hooks = {};
if (!w[NAMESPACE_IDENTIFIER].shims) w[NAMESPACE_IDENTIFIER].shims = [];

var namespace = w[NAMESPACE_IDENTIFIER];

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function define(prefix, icons) {
  var normalized = Object.keys(icons).reduce(function (acc, iconName) {
    var icon = icons[iconName];
    var expanded = !!icon.icon;

    if (expanded) {
      acc[icon.iconName] = icon.icon;
    } else {
      acc[iconName] = icon;
    }
    return acc;
  }, {});

  if (typeof namespace.hooks.addPack === 'function') {
    namespace.hooks.addPack(prefix, normalized);
  } else {
    namespace.styles[prefix] = _extends({}, namespace.styles[prefix] || {}, normalized);
  }

  if (prefix === 'fas') {
    define('fa', icons);
  }
}    

${icons_str}
${defines}
    
}());
    
`;

    // On écrit le fichier js
    fs.writeFile(path.resolve(current_path + '/' + output), new Buffer(js), "UTF-8", function(err) {
        if (err) {
            throw new EtdFaBuilderError(err);
        }
    });

}
