# etdfabuilder

Library to create custom build of FontAwesome 5.1

Add as dev dependencies :

```json
{
  "devDependencies": {
    "etdfabuilder": "git+https://gitlab.etd-solutions.com/etd-solutions/etdfabuilder.git#0.1.1"
  }
}
```

```bash
yarn install
```

To choose the icons to include in your bundle, add the following to `package.json` :

```json
{
  "etdfabuilder": {
    "free": {
      "brands": [
        "facebook",
        "github",
        "viadeo",
        "linkedin"
      ] 
    },
    "pro": {
      "regular": [
        "archive"
      ]
    }
  }
}
```

## CLI

```bash
etdfabuilder --output my-bundle.js
```
